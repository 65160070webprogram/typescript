let w: unknown = 1;
w = "String" ;
w = {
    runANoExistenMethod: () => {
        console.log("I think therefore I am");
    }
} as { runANoExistenMethod: () => void}

if(typeof w === 'object' && w!== null) {
    (w as { runANoExistenMethod: Function}).runANoExistenMethod();
}

